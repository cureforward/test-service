AuthService
======
This repository holds the implementation for a jwt authorization service to be used by our microservices

Build
-----
```
mvn install
```

Usage
-----
```
curl -H "AuthorizationJWT:<token>" <baseUrl>/v1/token/validate
```
Returns a 200 response if valid, else returns 401 unauthorized