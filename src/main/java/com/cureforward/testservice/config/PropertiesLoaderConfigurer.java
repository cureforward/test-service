package com.cureforward.testservice.config;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.StandardServletEnvironment;

/**
 * Created by michaelholland on 4/27/17.
 */
@Component
public class PropertiesLoaderConfigurer
        extends PropertySourcesPlaceholderConfigurer {

    private static final String ENVIRONMENT_PROPERTIES = "environmentProperties";

    private static final Logger LOGGER = Logger.getLogger(PropertiesLoaderConfigurer.class);

    @Override
    public void postProcessBeanFactory(
            final ConfigurableListableBeanFactory beanFactory)
            throws BeansException {

        super.postProcessBeanFactory(beanFactory);

        if(!(super.getAppliedPropertySources().get(ENVIRONMENT_PROPERTIES).getSource() instanceof StandardServletEnvironment)){
            return;
        }
        final StandardServletEnvironment propertySources =
                (StandardServletEnvironment) super.getAppliedPropertySources().get(ENVIRONMENT_PROPERTIES).getSource();

        propertySources.getPropertySources().forEach(propertySource -> {
            if (propertySource.getSource() instanceof Map) {
                // it will print systemProperties, systemEnvironment, application.properties and other overrides of
                // application.properties
                LOGGER.info("");
                LOGGER.info("Listing Properties from : " + propertySource.getName());
                LOGGER.info("");

                final Map<String, String> properties = mapValueAsString((Map<String, Object>) propertySource.getSource());
                for(String key : properties.keySet()){
                    LOGGER.info(key + " : "  + properties.get(key));
                }
            }
        });
    }

    private Map<String, String> mapValueAsString(
            final Map<String, Object> map) {

        return map.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> toString(entry.getValue())));
    }

    private String toString(
            final Object object) {

        return Optional.ofNullable(object).map(value -> value.toString()).orElse(null);
    }
}