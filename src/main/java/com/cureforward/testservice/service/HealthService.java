package com.cureforward.testservice.service;

import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthService {

  @RequestMapping(path = "/health", method = RequestMethod.GET)
  public ResponseEntity<String> getHealth() {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("message", "Health Check Success");
    return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);

  }

}
