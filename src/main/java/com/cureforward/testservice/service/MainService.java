package com.cureforward.testservice.service;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/tokens/_validate")
public class MainService {
  private final static Logger LOGGER = Logger.getLogger(MainService.class);

  @Value("${jwt.secret}")
  private String secret;

  @RequestMapping(path = "/", method = RequestMethod.GET)
  public ResponseEntity<String> validateToken(@RequestHeader("AuthorizationJWT") String auth) {
    long startTime = System.currentTimeMillis();

    try {
      Jwts.parser().setSigningKey(secret).parseClaimsJws(auth);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    catch (SignatureException ex) {
      LOGGER.error(String.format("Token validation failed due to invalid signature. token: %s ", auth), ex);
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    catch (ExpiredJwtException ex) {
      LOGGER.error(String.format("Token validation failed because token was expired. token: %s", auth), ex);
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    catch (Exception ex) {
      LOGGER.error("Runtime exception occurred: ", ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    finally {
      long endTime = System.currentTimeMillis();
      LOGGER.info(String.format("Authorization request completed in %d ms", endTime-startTime));
    }

  }

}