*** Settings ***
Documentation    Test Suite for Test Service
Library             HttpLibrary.HTTP

*** Variables ***
${hello_world_host}
${hello_world_path}     /v1/health/


*** Test Cases ***
Verify Hello World
    Create Http Context   ${hello_world_host}  http
    GET     ${hello_world_path}
    ${response}=            Get Response Status
    Should Start With   ${response}   200

