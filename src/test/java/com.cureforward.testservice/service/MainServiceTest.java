package com.cureforward.testservice.service;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(MainService.class)
public class MainServiceTest {

  @Autowired
  private MockMvc mockMvc;

  @Value("${jwt.secret}")
  private String VALID_KEY;

  private final String INVALID_KEY = "invalidKey";
  private final String VALIDATION_URL = "/tokens/_validate/";


  @Test
  public void testValidToken() throws Exception {
    String validToken = Jwts.builder()
        .setSubject("test")
        .signWith(SignatureAlgorithm.HS512, VALID_KEY)
        .compact();

    this.mockMvc.perform(get(VALIDATION_URL).header("AuthorizationJWT", validToken))
        .andExpect(status().isOk());
  }

  @Test
  public void testInvalidSignature() throws Exception {
    String invalidToken = Jwts.builder()
        .setSubject("test")
        .signWith(SignatureAlgorithm.HS512, INVALID_KEY)
        .compact();

    this.mockMvc.perform(get(VALIDATION_URL).header("AuthorizationJWT", invalidToken))
        .andExpect(status().isUnauthorized());
  }

  @Test
  public void testExpiredToken() throws Exception {
    String expiredToken = Jwts.builder()
        .setSubject("test")
        .setExpiration(new Date(System.currentTimeMillis()))
        .signWith(SignatureAlgorithm.HS512, VALID_KEY)
        .compact();

    this.mockMvc.perform(get(VALIDATION_URL).header("AuthorizationJWT", expiredToken))
        .andExpect(status().isUnauthorized());
  }

}
